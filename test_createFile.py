import unittest
from createFile import write_To_File

class Test_Create_File(unittest.TestCase):
	def test_small_size(self):
		assert write_To_File("c.txt",1024),"Invalid Value"
	def test_large_size(self):
		assert write_To_File("b.txt",268435456),"Invalid Value"
	def test_types(self):
		try:
			result= write_To_File([1,2,3],102)
			result= write_To_File((1,23),102)
			result= write_To_File(123,102)
			result= write_To_File(True,102)

			result= write_To_File("test.txt",[1,2,3])
			result= write_To_File("test.txt",(1,23))
			result= write_To_File("test.txt","123")
			result= write_To_File("test.txt",True)
		except Exception as e:
			assert isinstance(e,TypeError)
			

	def test_values(self):
		try:
			assert write_To_File("test.txt",-10)
		except Exception as e:
			assert isinstance(e,ValueError)
