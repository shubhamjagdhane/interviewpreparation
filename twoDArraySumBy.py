def printTwoD(td):
	i=0
	j=0
	l=list(map(lambda x:len(x),td))
	k=max(l)
	print("2D list: ")
	while i<k:
		while j<k: 
			print(td[i][j],end=" ")
			j+=1
		print()
		i+=1
		j=0
	ti=0
	tj=0
	i=0
	j=0
	cnt=0
	maxsum=0
	sume=0
	lcnt=0
	indexl=[]
	eleList=[]
	resultList=[]
	while i!=k-2:
		while j!=k-2:
			ti=i
			tj=j
			while True:
				if cnt==2:
					break;
				while lcnt<3:
					sume+=td[ti][tj]
					eleList.append(td[ti][tj])
					indexl = indexl + [[ti,tj]]
					tj+=1
					lcnt+=1
				cnt+=1
				ti=i+2;
				tj=j
				lcnt=0

			indexl+=[[i+1,j+1]]
			eleList.append(td[i+1][j+1])
			resultList.append(sum(eleList))
			eleList=[]
			print("\nIndex List= {}".format(indexl))
			
			indexl=[]
			cnt=0
			sume+=td[i+1][j+1]
#			print("Sum= {}".format(sume))
			if sume>maxsum:
				maxsum=sume
			j+=1
			sume=0
		j=0
		i+=1
#	print("Maximum sum= {}".format(maxsum))
	return max(resultList)

if __name__=='__main__':
#	td=[[1,1,1,0,0,0],[0,1,0,0,0,0],[1,1,1,0,0,0],[0,0,2,4,4,0],[0,0,0,2,0,0],[0,0,1,2,4,0]]
	td=[[-1,-1,0,-9,-2,-2],[-2,-1,-6,-8,-2,-5],[-1,-1,-1,-2,-3,-4],[-1,-9,-2,-4,-4,-5],[-7,-3,-3,-2,-9,-9],[-1,-3,-1,-2,-4,-5]]
	print("Maximum sum = {}".format(printTwoD(td)))
