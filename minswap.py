#use insertion sort

def minimumSwap(arr,n):
	asort=sorted(arr)
	i=0
	cnt=0
	#for i in range(n):
	while i!=n-1:
		if arr==asort:
			break
		if (i+1!=arr[i]):
			#j=arr.index(i+1)
			t=arr[i]
			#arr[i]=arr[arr[i]]
			arr[i]=arr[t-1]
			arr[t-1]=t
			cnt+=1
			i-=1
			print(arr)
		i+=1
	return cnt

if __name__=='__main__':
	n=int(input())
	l=list(map(int,input().split()))
	res=minimumSwap(l,n)
	print("Minimum Swaps:",res)
