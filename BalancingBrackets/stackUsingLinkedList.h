#include<stdio.h>
#include<stdlib.h>

typedef struct StructStack{
	char data;
	struct StructStack *next;
}Stack;

int isEmptyStack(Stack *S);
void deleteStack(Stack **S);
Stack *createStack(void);
char Pop(Stack **top);
void Push(Stack **top,char ch);

/*int main(void){
	int choice;
	char data;
	Stack *S=createStack();
	while(1){
		printf("\n1: Push\n2: Pop\n3: Top\n4: Exit");
		printf("\nEnter your choice: ");
		scanf("%d",&choice);
		if(choice==4)
			break;
		switch(choice){
			case 1:
				getchar();
				printf("\nEnter your data: ");
				scanf("%c",&data);
				Push(&S,data);
				break;
			
			case 2:
				if(isEmptyStack(S))
					printf("\n\t\t***Stack is already Empty***");
				else
					printf("\n\t\t***Popped Element: %c***",Pop(&S));
				break;
			case 3:
				if(isEmptyStack(S))
					printf("\n\t\t***Stack is Empty***");
				else
					printf("\n\t\t***Top Element= %c***",S->data);
		}
	}
	deleteStack(&S);
	if(!S)
		printf("\n\t\t***Stack is successfully deleted***");
	printf("\n\n");
}
*/

void deleteStack(Stack **S){
	Stack *temp=*S;
	while(*S!=NULL){
		*S=(*S)->next;
		free(temp);
		temp=*S;
	}
	*S=NULL;
}

Stack *createStack(void){
	Stack *S;
	S=NULL;
	return S;
}

void Push(Stack **top,char ch){
	Stack *newNode=(Stack *)malloc(sizeof(Stack));
	newNode->data=ch;
	newNode->next=NULL;

	if(*top==NULL)
		*top=newNode;
	else{
		newNode->next=*top;
		*top=newNode;
	}
}

char Pop(Stack **top){
	char data='1';
	if(!isEmptyStack(*top)){
		Stack *temp=*top;
		if((*top)->next)
			*top=(*top)->next;
		else
			*top=NULL;
		data=temp->data;
		free(temp);
	}
	return data;
}

int isEmptyStack(Stack *S){
	return (S==NULL);
}
