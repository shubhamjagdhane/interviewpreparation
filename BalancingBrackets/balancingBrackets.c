#include<stdio.h>
#include<stdlib.h>
#include "stackUsingLinkedList.h"
#define SIZE 100

int isBalancingBrackets(char *str);

int main(void){
	char *str=(char *)malloc(sizeof(char)*SIZE);

	printf("\nEnter your string: ");
	fgets(str,SIZE,stdin);

	if(isBalancingBrackets(str))
		printf("\n\t\t***Valid Parentheses***");
	else
		printf("\n\t\t***Invalid parantheses***");
	free(str);
	printf("\n\n");
}

int isBalancingBrackets(char *str){
	int i=0;
	Stack *S=createStack();
	while(str[i]!='\n' && str[i]!='\0'){
		if(str[i]=='{' || str[i]=='(' || str[i]=='[' || str[i]=='<')
			Push(&S,str[i]);
		else if(S!=NULL && ((str[i]==')' && S->data=='(') || (str[i]=='}' && S->data=='{') || (str[i]==']' && S->data=='[') || (str[i]=='>' && S->data=='<')))
			Pop(&S);
		else
			return 0;

		i+=1;
	}
	if(S==NULL)	
		return 1;
	else{
		deleteStack(&S);
		return 0;
	}
}
