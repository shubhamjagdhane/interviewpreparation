def getSumWithRange(l1,left,right):
	sumlist=[]
	l1LR=[]
	l1RL=[]
	ans=0
	l=0
	for i in l1: #creating Left to Right list
		ans+=i
		l1LR.append(ans)
		l+=1
	i=l-1
	ans=0

	while i>=0: #creating Right to Left list
		ans+=l1[i]
		l1RL.append(ans)
		i-=1

	ans=0
	for i in range(len(left)):
		le=left[i]
		re=right[i]
		start=(le%l)
		end=(re%l)
#		d=re-le-start-end
		d=re-le
		timesmultiple=d//l
#		print("start={} end={} d={} tm={}".format(start,end,d,timesmultiple))
		ans=l1RL[-start]+l1LR[end-1]+(timesmultiple*l1LR[-1])
		sumlist.append(ans)

	return sumlist

if __name__=='__main__':
	l1=list(map(int,input("Enter Your list: ").split()))
	left=list(map(int,input("Enter left indices for list: ").split()))
	right=list(map(int,input("Enter right indices for list: ").split()))
	print("Sum of list with respect to indices= {}".format(getSumWithRange(l1,left,right)))

