from flask import Flask
import json

app = Flask(__name__)

@app.route("/")
def helloWorld():
	return "Hello World"

@app.route("/movies")
def movies():
	a = dict(
		name="Abc",
		director="Pqr",
		duration=190
	)
	return json.dumps(a) 

@app.route("/test")
def test():
	a="This is Test for printing"
	return json.dumps(a)
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5000)
