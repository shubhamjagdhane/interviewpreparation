#include<stdio.h>
#include<stdlib.h>

typedef struct StructTree{
	int data;
	struct StructTree *left,*right;
}Tree;

Tree *createTreeBySortedArray(int *arr,int start,int end);
void PreOrder(Tree *root);
Tree *newNode(int data);
int main(void){
	int arr[] = {1,2,3,4,5,6,7};
	int n=7;
	Tree *root=createTreeBySortedArray(arr,0,n-1);
	printf("\nBalance Binary Tree: ");
	PreOrder(root);
	printf("\n");
}

Tree *createTreeBySortedArray(int *arr,int start,int end){
	if(start>end)
		return NULL;
	int mid=(start+end)/2;

	Tree *root=newNode(arr[mid]);

	root->left=createTreeBySortedArray(arr,start,mid-1);
	root->right=createTreeBySortedArray(arr,mid+1,end);
	return root;
}

void PreOrder(Tree *root){
	if(root){
		printf("%d ",root->data);
		PreOrder(root->left);
		PreOrder(root->right);
	}
}

Tree *newNode(int data){
	Tree *node=(Tree *)malloc(sizeof(Tree));
	node->data=data;
	node->left=node->right=NULL;
	return node;
}
