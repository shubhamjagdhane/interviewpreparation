import unittest
from copyFile import write_File

class TestCopyFile(unittest.TestCase):
	def test_small_file_size(self):
		result=write_File("small.txt","a.txt")
		assert result,"Incorrect Output"
	def test_large_file_size(self):
		result=write_File("large.txt","b.txt")
		assert result,"Incorrect Output"

	def test_file_does_not_exist(self):
		try:
			result=write_File("notexists.txt","c.txt")
		except Exception as e:
			assert isinstance(e,FileNotFoundError)

	def test_type_input(self):
		try:
			result=write_File([1,2,3],"t1.txt")
			result=write_File((1,2,3),"t1.txt")
			result=write_File(123,"t1.txt")
			result=write_File(12.13,"t1.txt")
			result=write_File({1:2,3:12},"t1.txt")
			result=write_File(-123,"t1.txt")
			
			result=write_File("t2.txt",[1,2,3])
			result=write_File("t2.txt",(1,2,3))
			result=write_File("t2.txt",123)
			result=write_File("t2.txt",12.13)
			result=write_File("t2.txt",{1:2,3:12})
			result=write_File("t2.txt",-123)
		except Exception as e:
			assert isinstance(e,TypeError)

		
