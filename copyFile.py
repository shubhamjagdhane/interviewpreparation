import os.path
from os import path

def write_File(source_file,dest_file):
	try:
		if path.exists(source_file):
			if type(source_file)!=str and type(dest_file)!=str:
				raise TypeError()
			with open(source_file,"rb") as s ,open(dest_file,"wb") as d:
				while True:
					chunk = s.read(1024)
					if len(chunk)==0:
						break
					d.write(chunk)

			sfile = os.stat(source_file)
			dfile = os.stat(dest_file)
			if sfile.st_size==dfile.st_size:
				return True
			else:
				return False
		else:
			raise FileNotFoundError("Source File is not exists")

	except TypeError:
		raise TypeError("Invalid Type, File name should in string format")

def copyFile(source_file,dest_file,dpath):
	try:
		if type(source_file)!=str and type(dest_file)!=str:
			raise TypeError()
		if path.exists(source_file):
			if not path.isdir(dpath):
				raise FileNotFoundError("Invalid Path")
			if dpath=='.':
				dpath=path.dirname(path.abspath(source_file))
			dest_file=dpath+"/"+dest_file
			if path.exists(dest_file):
				print("{} file is already present in given location".format(dest_file))
				check=input("Do you want to overwrite??(Y/N)")
				if check.lower()=="y" or check.lower()=="yes":
					return write_File(source_file,dest_file)
				else:
					return False
			else:
				return write_File(source_file,dest_file)

		else:
			raise FileNotFoundError("File does not exists")
	except TypeError:
		raise TypeError("Invalid Type,File name should in string form")
	except:
		raise("Something went wrong.!!")

if __name__=='__main__':
	sfile=input("Enter source file name: ")
	dfile=input("Enter destination file name: ")
	dpath=input("Enter the absolute path of destination file\n. for current directory\nYour Path: ")
	if copyFile(sfile,dfile,dpath):
		print("File is successfully created")
	else:
		print("File is not created")
