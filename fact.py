from functools import reduce
def findFact(n):
#if type(n) != int:
#		raise TypeError("Invalid type. Expected int, got {}".format(type(n)))
#	if n < 0:
#		raise ValueError("Invalid value. Expected positive integer, got negative integer")
#	if n==0 or n==1:
#		return 1
#	elif n>1:
#		return reduce(lambda x,y: x*y, range(2,n+1))

	try:
#		n=int(n)
		if type(n)!=int:
			raise TypeError()
		if n<0:
			raise ValueError()
		if n==0 or n==1:
			return 1
		elif n>1:
			return reduce(lambda x,y:x*y,range(2,n+1))
	except TypeError:
		print("\nInvalid Type,Need Integer Type")
	except ValueError:
		print("\nInvalid Value.Please Entered Positive Integer")
	except:
		print("Somthing went very wrong")

if __name__ == '__main__':
	str=input("Enter Your value: ");
	print(findFact(str) if findFact(str) else "")

