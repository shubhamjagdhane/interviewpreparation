#include<stdio.h>
#include<stdlib.h>

typedef struct StructTree{
	int data;
	struct StructTree *down;
}Tree;

typedef struct StructList{
	Tree *node;
	struct StructList *next;
}List;
Tree *DeleteList(List *head);
Tree *DeleteNArrayTree(Tree *root);
List *createListForNode(int data,int n);
Tree *createNArrayTree(void);
void printLinkedList(List *head);
void printNArrayTree(Tree *root);
Tree *searchInList(List *head,int data);

int main(void){
	Tree *root=NULL;
	root=createNArrayTree();
	printNArrayTree(root);
	root=DeleteNArrayTree(root);
	if(!root)
		printf("\nTree is successfully deleted..!!");
	printf("\n\n");
}

void printNArrayTree(Tree *root){
	printf("\n\nTree: %d ",root->data);
	
	if(root->down){
		printLinkedList((List *)root->down);
	}
}

void printLinkedList(List *head){
	List *p=head;
	while(p!=NULL){
		printf("%d ",p->node->data);
		if(p->node->down)
			printLinkedList((List *)p->node->down);
		p=p->next;
	}
}

Tree *createNArrayTree(void){
	Tree *root=NULL,*newNode,*present=NULL;
	List *head=NULL,*Lnn,*p;
	int n,cn,ych;

/*	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	for(int i=0 ; i<n ; i++){*/
	while(1){
		if(root==NULL){
			newNode=(Tree *)malloc(sizeof(Tree));
			printf("\nEnter data for Root Node: ");
			scanf("%d",&newNode->data);
			newNode->down=NULL;
			printf("\nHow many child node you want for %d node: ",newNode->data);
			scanf("%d",&cn);
			newNode->down=(Tree *)createListForNode(newNode->data,cn);
			root=newNode;
		}
		else{
			printf("\nYou can insert into following nodes only: ");
			printLinkedList((List *)root->down);
			printf("\nEnter your choice(-1 to Exit): ");
			scanf("%d",&ych);
			if(ych==-1)
				break;
			present=searchInList((List *)root->down,ych);
			if(!present){
				printf("\nYou have entered wrong data..!!");
				continue;
			}
			else{
/*				char ch;
				if(present->down){
					printf("\n%d node already has children(s): ",ych);
					printf("\nDo you want to overwrite?(Y/N)");
					scanf("%c",&ch);

				}
				if(ch!='N'){*/
					printf("\nHow many child node you want for %d node: ",present->data);
					scanf("%d",&cn);
					present->down=(Tree *)createListForNode(present->data,cn);
//				}
			}
		}
	}
	return root;
}

Tree *searchInList(List *head,int data){
	Tree *temp=NULL;
	List *p=head;
	int flag=0;
	while(p!=NULL){
		if(p->node->data==data){
			flag=1;
			break;
		}
		if(p->node->down){
			temp=searchInList((List *)p->node->down,data);
			if(temp)
				return temp;
		}
		p=p->next;
	}
	if(flag==1)
		return p->node;
	else 
		return NULL;
}

List *createListForNode(int data,int n){
	List *head=NULL,*newNode,*p;
	for(int i=0 ; i<n ; i++){
		newNode=(List *)malloc(sizeof(List));
		newNode->node=(Tree *)malloc(sizeof(Tree));
		printf("\nEnter data for child no %d of %d node: ",i+1,data);
		scanf("%d",&newNode->node->data);
		newNode->node->down=NULL;
		newNode->next=NULL;
		if(head==NULL){
			head=newNode;
		}
		else{
			p=head;
			while(p->next!=NULL){
				p=p->next;
			}
			p->next=newNode;
		}
	}
	return head;
}

Tree *DeleteNArrayTree(Tree *root){
	List *temp=(List *)root->down,*p;
	while(temp){
		if(temp->node->down){
			temp->node->down=DeleteList((List *)temp->node->down);
		}
		p=temp;
		free(p);
		temp=temp->next;
	}
	free(temp);
	free(root);
	return NULL;
}

Tree *DeleteList(List *head){
	List *temp=head;
	while(temp){
		head=head->next;
		free(temp);
		temp=head;
	}
	return NULL;
}
