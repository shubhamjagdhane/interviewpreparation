import cv2
import numpy as np
import sys

image=cv2.imread(sys.argv[1])
#cv2.imshow("Original",image)
#blurred=cv2.pyrMeanShiftFiltering(image,11,31)
gray= cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
#cv2.imshow("Gray",gray)
ret , threshold= cv2.threshold(gray,127,255,1)

cv2.imshow("Old Threshold",threshold)
contours, _ =cv2.findContours(threshold,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)

print("Number of the contours detected:",len(contours))

#cv2.drawContours(gray,contours,-1,(0,0,255),10)

"""for cnt in contours:
	rect = cv2.minAreaRect(cnt)
	box=cv2.boxPoints(rect)
	box=np.int0(box)
	image=cv2.drawContours(image,[box],0,(0,0,255),3)
cv2.imshow("boxed image",image)"""

for cnt in contours:
	epsilon=0.01*cv2.arcLength(cnt,True)

	approx=cv2.approxPolyDP(cnt,epsilon,True)

	gray=cv2.drawContours(gray,[approx],0,(0,255,0),15)


ret , threshold= cv2.threshold(gray,127,255,1)
cv2.imshow("Threshold",threshold)
#contours,h =cv2.findContours(threshold,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
#print("h=",h,"len(h)=",len(h))
contours,h =cv2.findContours(threshold,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

sq=0
#for cnt in contours:
l=[]
for i in range(len(contours)):
	cnt=contours[i]
	epsilon=0.01*cv2.arcLength(cnt,True)
	approx=cv2.approxPolyDP(cnt,epsilon,True)
	if len(approx)==4:
		#print("Approx:",approx)
		
		sq+=1
		#cv2.drawContours(gray,[cnt],0,(0,0,255),-1)
		print("h:",h[0][i])
		if h[0][i][3]!=-1 and h[0][i][2]!=-1:
			cv2.drawContours(gray,[cnt],0,(0,0,255),-1)
			l.append(sq-1)
			#print("h:",h[0][i])"""
		"""if sq==2:
			cv2.drawContours(gray,[cnt],0,(0,0,255),-1)
			break"""

print("Cells are:",l)
print("Total Number of sqaures:",sq)
cv2.imshow("Testing",gray)
cv2.waitKey(0)
cv2.destroyAllWindows()
