def subArraySum(l1,asum):
	start=0
	end=0
	rsum=0
	for i in range(len(l1)):
		rsum+=l1[end]
		if rsum==asum:
			return [start+1,end+1]
		elif rsum<asum:
			end+=1
		else:
			while rsum>asum:
				rsum-=l1[start]
				start+=1
			end+=1
			if rsum==asum:
				return [start+1,end]
	return [-1]
if __name__=='__main__':
	n=int(input())
	for i in range(n):
		tip=list(map(int,input().split()))
		lsize=tip[0]
		lsum=tip[1]
		l1=list(map(int,input().split()))
		result=subArraySum(l1,lsum)
		for j in result:
			print(j,end=" ")
		print("")	
