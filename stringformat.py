# Arrange the given words from string with respect to length of words if the length of word is same then keep it with which comes first.
#First word's initial must be capitalized and new string should ends with full stop(.)

def addToDictionary(liststring):
	d={}
	for i in liststring:
		if(d.get(len(i))):
			d[len(i)] = d[len(i)]+[i]
		else:
			d[len(i)]=[i]
	return d

def getKeysList(d):
	dlist=[]
	for i in d:
		dlist=dlist+[i]

	dlist=sorted(dlist)
	return dlist

def updateDictionary(d,keyslist):
	list1 = d[keyslist[0]]
	str1=list1[0]
	str1=str1.capitalize()
	list1[0]=str1
	d[keyslist[0]]=list1
	return d

def finalStringInLOL(d,keyslist):
	newstrlist=[]
	for i in keyslist:
		newstrlist=newstrlist+[d[i]]
	return newstrlist

def getFinalString(LOL):
	newstr=""
	for i in LOL:
		for j in i:
			newstr=newstr+j+" "
	newstr=newstr[:len(newstr)-1]+"."
	return newstr

def newstring(string):
	string=string[:len(string)]
	liststring=[]
	string=string.lower()
	liststring=string.split()
	d={}
	d=addToDictionary(liststring)
	keyslist=getKeysList(d)

	d=updateDictionary(d,keyslist)
	fLOL=finalStringInLOL(d,keyslist)
	string=getFinalString(fLOL)
	return string

string=input("Enter your string: ")
string=newstring(string)
print("Your New String: {}".format(string))
