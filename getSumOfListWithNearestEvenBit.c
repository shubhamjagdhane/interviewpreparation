/*
Work: An array having number integer greater than or equal to -1. Your job is give the sum of list in case if you get -1 number then replace that number by previous nearest to even bit number.
Even Bit Number= Number which all even bit are ON.

I/P: 
	1)Number of testcases
	2)Size of an array/list
	3)Enter number of elements which are equal to size of list with number containing -1 element.
	
O/P:
	Sum of array in which -1 is replace with nearest even bit number.

   

*/


#include<stdio.h>
#include<stdlib.h>

int giveNextEvenBitNumber(int n);
int *reverse(int *arr,int n);
int power(int a,int b);
int getNumber(int *arr,int n);
int check(int *arr,int n);
int getMaximumSum(int *arr,int n);

int main(void){
	int number,ntest;
	int *array;
	printf("\nEnter number of test cases: ");
	scanf("%d",&ntest);
	for(int i=0 ; i<ntest ; i++){
		printf("\nEnter size of a list: ");
		scanf(" %d",&number);
		array=(int *)malloc(sizeof(int)*number);
		for(int j=0 ; j<number ; j++){
			scanf("%d",&array[j]);
		}
		printf("\nMaximum sum of array= %d",getMaximumSum(array,number));
	}
	printf("\n\n");
}

int giveNextEvenBitNumber(int n){
	int *bin=(int *)malloc(sizeof(int)*23);
	int i=0,totbin;
	while(n>0){
		bin[i]=n%2;
		i++;
		n=n/2;
	}
	totbin=i;
	bin=reverse(bin,totbin);
	totbin=check(bin,totbin);
	return totbin;
}

int *reverse(int *arr,int n){
	int temp;
	for(int i=0 ,j=n-1 ; i<j ; i++,j--){
		temp=arr[i];
		arr[i]=arr[j];
		arr[j]=temp;
	}
	return arr;
}
int power(int a,int b){
	int ans=1;
	for(int i=0 ; i<b ; i++){
		ans=ans*a;
	}
	return ans;
}
int getNumber(int *arr,int n){
	int sum=0;
	for(int i=n-1 ,j=0; i>=0 ; i--,j++){
		sum=sum+(arr[j]*power(2,i));
	}
	return sum;
}
int check(int *arr,int n){
	int prevNumber=getNumber(arr,n),newNumber;
	if(n%2==0){
		int i;
		for(i=1 ; i<n ; i=i+2){
			if(arr[i]==1){
				arr[i]=0;
				arr[i-1]=1;
				break;
			}
		}
		for(int j=i ; j<n ; j++){
			arr[j]=0;
		}
		newNumber=getNumber(arr,n);
		if(newNumber<prevNumber){
			free(arr);
			n=n+2;
			arr=(int *)malloc(sizeof(int)*n);
			arr[0]=1;
			for(int i=1; i<n; i++){
				arr[i]=0;
			}

			return (getNumber(arr,n));
		}
			
	}
	else{
		if(arr[0]==1){
			free(arr);
			n=n+1;
			arr=(int *)malloc(sizeof(int)*n);
			arr[0]=1;
			for(int i=1 ; i<n ; i++){
				arr[i]=0;
			}
		}
		else{
			for(int i=0 ; i<n ; i=i+2){
				if(arr[i]==1){
					arr[i]=0;
					arr[i-1]=1;
				}
			}
		}
	}
	return (getNumber(arr,n));
}
int getMaximumSum(int *arr,int n){
	int sum=0,num;
	for(int i=0 ; i<n ; i++){
		if(arr[i]==-1){
			sum+=giveNextEvenBitNumber(arr[i-1]);
		
		}
		else{
			sum+=arr[i]; 
		}
	}
	return sum;
}

