#include<stdio.h>
#include<stdlib.h>
//#include "queueForTree.h"

typedef struct StructTreeForTest{
	int data;
	struct StructTreeForTest *left,*right;
}TreeTest;

typedef struct StructListStack{
	TreeTest *node;
	struct StructListStack *next;
}ListStack;

typedef struct StructStack{
	ListStack *top;
}Stack;


int isEmptyStack(Stack *S);
TreeTest *Top(Stack *S);
TreeTest *Pop(Stack *S);
void Push(Stack *S,TreeTest *node);
void DeleteStack(Stack *S);
Stack *createStack(void);
void PreOrderNonRecurrsive(TreeTest *root);
void InOrderNonRecurrsive(TreeTest *root);

/*int main(void){
	int data;
	TreeTest *root=NULL;
	printf("\nEnter how many nodes for tree: ");
	scanf("%d",&data);
	root=(TreeTest*)createTreeByLevelOrder(data);
	PreOrderNonRecurrsive(root);
	InOrderNonRecurrsive(root);
	printByLevelOrder((Tree *)root);
	DeleteTree((Tree *)root);
	printf("\n\n");
}*/

Stack *createStack(void){
	Stack *S=(Stack *)malloc(sizeof(Stack));
	ListStack *temp=(ListStack *)malloc(sizeof(ListStack));
	S->top=NULL;
	return S;
}

void Push(Stack *S,TreeTest *node){
	ListStack *listNode=(ListStack *)malloc(sizeof(ListStack));
	listNode->node=node;
	listNode->next=NULL;
	
	if(S->top)
		listNode->next=S->top;
	S->top=listNode;
}

TreeTest *Pop(Stack *S){
	int data=-1;
	ListStack *temp;
	TreeTest *tempNode;
	if(isEmptyStack(S))
		return NULL;
	else{
		temp=S->top;
		if(S->top->next)
			S->top=S->top->next;
		else
			S->top=NULL;
		tempNode=temp->node;
		free(temp);
		return tempNode;
	}
}

TreeTest *Top(Stack *S){
	if(S->top)
		return S->top->node;
	else
		return NULL;
}

int isEmptyStack(Stack *S){
	return (S->top==NULL);
}

void DeleteStack(Stack *S){
	ListStack *temp;
	while(S->top){
		temp = S->top;
		S->top=S->top->next;
		free(temp);
	}
	free(S);
}

void PreOrderNonRecurrsive(TreeTest *root){
	Stack *S=createStack();
	printf("\nPreorder-Non-Recurrsive: ");
	while(1){
		while(root){
			printf("%d ",root->data);
			Push(S,root);
			root=root->right;
		}
		if(isEmptyStack(S))
			break;
		root=Pop(S);
		root=root->left;
	}
	DeleteStack(S);
}

void InOrderNonRecurrsive(TreeTest *root){
	Stack *S=createStack();
	printf("\nInorder-Non-Recurrsive: ");
	while(1){
		while(root){
			Push(S,root);
			root=root->right;
		}
		if(isEmptyStack(S))
			break;
		root=Pop(S);
		printf("%d ",root->data);
		root=root->left;
	}
	DeleteStack(S);
}
