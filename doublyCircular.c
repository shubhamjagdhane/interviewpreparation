#include<stdio.h>
#include<stdlib.h>

typedef struct StructList{
	int data;
	struct StructList *prev,*next;
}Dlist;

void displayDlist(Dlist *head);
Dlist *createDlist(int n);
void swap(Dlist *head);

int main(void){
	int n;
	printf("\nEnter how many elements: ");
	scanf("%d",&n);

	Dlist *head=NULL;
	head=createDlist(n);

	printf("\nOriginal List: ");
	displayDlist(head);

	swap(head);
	printf("\nAfter Swapping: ");
	displayDlist(head);
	printf("\n\n");
}

void swap(Dlist *head){
	Dlist *temp1=head,*temp2=head->prev;
	int temp;
	while(1){
		temp=temp1->data;
		temp1->data=temp2->data;
		temp2->data=temp;



		temp1=temp1->next;
		temp2=temp2->prev;


		if(temp1==temp2 || temp2->next==temp1)
			break;
	}

}
Dlist *createDlist(int n){
	Dlist *newNode,*head=NULL,*temp;
	for(int i=0 ; i<n ; i++){
		newNode=(Dlist *)malloc(sizeof(Dlist));
		printf("\nEnter data: ");
		scanf("%d",&newNode->data);
		if(head==NULL){
			head=newNode;
			newNode->next=head;
			head->prev=newNode;
		}
		else{
			temp=head;
			while(temp->next!=head){
				temp=temp->next;
			}
			temp->next=newNode;
			newNode->prev=temp;
			newNode->next=head;
			head->prev=newNode;
		}
	}
	return head;
}

void displayDlist(Dlist *head){
	Dlist *temp=head;
	do{
		printf("%d ",temp->data);
		temp=temp->next;
	}while(temp!=head);
}
