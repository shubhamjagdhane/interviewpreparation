import os.path
from os import path

def getNearestPowerOfTwo(n):
	cnt=0
	binlist=[]
	while n>0:
		r = n%2
		binlist.append(r)
		n=n//2
	binlist.reverse()
	print("Nearest Power of two={}".format(binlist))

def generateExponentially(poweroftwo):
	poweroftwo=int(poweroftwo)
	stradd="a"
	poweroftwo=2**poweroftwo
	stradd=stradd*poweroftwo
	return stradd

def write_To_File(filename,size):
	try:
		size=int(size)
		if type(filename)!=str and type(size)!=int:
			raise TypeError("Invalid Type")
		if size<0:
			raise ValueError("Invalid Value,Positive Size Expected")
		a="a"
		magic_size=134217728
		with open(filename,"w") as f:
			if size>magic_size:
				a=a*magic_size
				rem_data=size%magic_size
				tot_chunks=size//magic_size
				for i in range(tot_chunks):
					f.write(a)
				if rem_data:
					a="a"
					f.write(a*rem_data)
			else:
				f.write(a*size)

		return True
	except TypeError:
		raise TypeError("Invalid Type")
	except ValueError:
		raise ValueError("Invalid Value,Postive Size Expected")
	except:
		raise("Something went wrong")

def createFile(filename,size):
	try:
		if path.exists(filename):
			print("File is already present.")
			ch=input("Do you still want to overwrite?(Y?N)")
			if ch=="Y" or ch=="Yes" or ch=="YES" or ch=="y":
				return write_To_File(filename,size)
			else:
				return False
		else:
			return write_To_File(filename,size)
	except:
		raise("Something Went Wrong")

if __name__=='__main__':
	fname=input("Enter filename: ")
	size=input("Enter Require size of file: ")
	if createFile(fname,size):
		print("File is successfully created.")
	else:
		print("File is not created.")
