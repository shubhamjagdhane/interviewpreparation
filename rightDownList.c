#include<stdio.h>
#include<stdlib.h>

typedef struct StructSpecialLinkedList{
	int data;
	struct StructSpecialLinkedList *next,*down;
}SpecialLL;
SpecialLL *createList(int n);
SpecialLL *searchNode(SpecialLL *head,int data);
void deleteList(SpecialLL *head);
void displayAllList(SpecialLL *head);
void displayMainList(SpecialLL *head);
int displayForDeletion(SpecialLL *head);

int main(void){
	int n,ch,data;
	SpecialLL *head=NULL,*temp;

	do{
		printf("\n1: CreateList\n2: Insert list down to given node\n3: Delete Particular List\n4: Display Special List\n5: Exit");
		printf("\nEnter your choice: ");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				printf("\nEnter how many elements for a Your Main list: ");
				scanf("%d",&n);
				head=createList(n);
				break;

			case 2:
				printf("\n***Your Are Allow to insert down to only below nodes***\n");
				displayMainList(head);
				printf("\nEnter your node for down insertion: ");
				scanf("%d",&data);
				temp=searchNode(head,data);
				if (temp){
					printf("\nHow many nodes you want to insert down to %d node: ",data);
					scanf("%d",&n);
					temp->down=createList(n);
				}
				else
					printf("\nNode does not exist, Try another Node...!!!");
				break;

			case 3:	
				n=displayForDeletion(head);
				if(n){
					printf("\nEnter your node for down deletion of list: ");
					scanf("%d",&data);
					temp=searchNode(head,data);
					if(temp){
						deleteList(temp->down);
						temp->down=NULL;
					}
					else
						printf("\nNode does not exist, Try another Node...!!!");
				}
				else
					printf("\n\t***No Node for Deletion..!!***");
				break;
			case 4:
				printf("\nYour Created List: ");
				displayAllList(head);
				break;
		}
	}while(ch!=5);
	deleteList(head);
}

SpecialLL *createList(int n){
	SpecialLL *head=NULL,*newNode,*p;
	for(int i=0 ; i<n ; i++){
		newNode=(SpecialLL *)malloc(sizeof(SpecialLL));
		printf("\nEnter your data for node: ");
		scanf("%d",&newNode->data);
		newNode->next=NULL;
		newNode->down=NULL;
		if(head==NULL){
			head=newNode;
		}
		else{
			p=head;
			while(p->next!=NULL){
				p=p->next;
			}
			p->next=newNode;
		}
	}
	return head;
}

SpecialLL *searchNode(SpecialLL *head,int data){
	SpecialLL *temp=head;
	while(temp!=NULL){
		if(temp->data==data)
			break;
		temp=temp->next;
	}
	return temp;
}

void deleteList(SpecialLL *head){
	SpecialLL *temp;
	while(head!=NULL){
		temp=head;
		if(temp->down)
			deleteList(temp->down);
		head=head->next;
		free(temp);
	}
}

void displayMainList(SpecialLL *head){
	SpecialLL *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
	printf("\b\b\n");
}

int displayForDeletion(SpecialLL *head){
	SpecialLL *temp=head;
	int check=0;
	printf("\n\n");
	while(temp!=NULL){
		if(temp->down){
			printf("%d ",temp->data);
			check=1;
		}
		temp=temp->next;
	}
	if(check)
		printf("\n***Your Are Allow to delete down to only above nodes***\n");
	return check;
}

void displayAllList(SpecialLL *head){
	SpecialLL *temp=head;
	while(temp!=NULL){
		printf("%d ",temp->data);
		if(temp->down)
			displayAllList(temp->down);
		temp=temp->next;
	}
}
