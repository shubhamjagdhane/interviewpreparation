def arithmatic(a,b):
	try:
		a=int(a)
		b=int(b)
		print("a/b=",a/b)
		print("a+b=",a+b)
	except ValueError:
		print("Could not convert to a number.")
	except ZeroDivisionError:
		print("Can't divide by zero")
	except:
		print("Something went very wrong")


if __name__=="__main__":
	a=input("1st number: ")
	b=input("2nd number: ")
	arithmatic(a,b)
