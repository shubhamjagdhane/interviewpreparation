#Find prime numbers upto n in O(sqrt(n)) of time complexity

def getPrimeNumbers(n):
	n=int(n)
	if n<0:
		raise ValueError("n should be positive")
	pl=[0,0]
	pl=pl+list(map(lambda x:x,range(2,n+1)))
	i=2
	while i*i<=n:
		if pl[i]!=0:
			for j in range(2,n):
				if pl[i]*j>n:
					break;
				else:
					pl[pl[i]*j]=0
		i+=1
	pl = list(filter(lambda x:x,pl))
	return pl

if __name__=='__main__':
	n=input("Enter n to find up to n's prime number(s): ")
	print(getPrimeNumbers(n))
