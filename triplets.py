def triplet(l1):
	l1.sort()
	print("Sorted=",l1)
	l=len(l1)
	left=0
	right=0
	cnt=0
	limit=l-1
	while limit>=2:
		left=0
		right=limit-1
		while(left<right):
			pairs=l1[left]+l1[right]
			if pairs==l1[limit]:
				cnt+=1
			if pairs<l1[limit]:
				left+=1
			else:
				right-=1
		limit-=1
	return cnt

if __name__=='__main__':
	l1=list(map(int,input().split()))
	print("Total Number of triplet=",triplet(l1))
