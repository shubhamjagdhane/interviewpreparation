#include<stdio.h>
#include<stdlib.h>

typedef struct StructList{
	int data;
	struct StructList *next;
}List;

typedef struct StructStack{
	List *top;
}Stack;


int isEmptyStack(Stack *S);
int Top(Stack *S);
int Pop(Stack *S);
void Push(Stack *S,int data);
void DeleteStack(Stack *S);
Stack *createStack(void);

/*int main(void){
	int n,data;
	Stack *S=createStack();
	do{
		printf("\n1: Push\n2: Pop\n3: Top\n4: IsEmptyStack\n5: Exit");
		printf("\nEnter your choice: ");
		scanf("%d",&n);
		switch(n){
			case 1:
				printf("\nEnter data for push: ");
				scanf("%d",&data);
				Push(S,data);
				break;

			case 2:
				data=Pop(S);
				if(data!=-1)
					printf("\nPopped Element= %d",data);
				else
					printf("\nStack is Empty");
				break;

			case 3:
				if(Top(S)==-1)
					printf("\nStack is Empty..!!");
				else
					printf("\nTop Element= %d",S->top->data);
				break;
			case 4:
				if(isEmptyStack(S))
					printf("\nStack is Empty..!!");
				else
					printf("\nStack is not Empty..!!");
				break;
			}
	}while(n!=5);
	DeleteStack(S);
}*/

Stack *createStack(void){
	Stack *S=(Stack *)malloc(sizeof(Stack));
	List *temp=(List *)malloc(sizeof(List));
	S->top=NULL;
	return S;
}

void Push(Stack *S,int data){
	List *node=(List *)malloc(sizeof(List));
	node->data=data;
	node->next=NULL;

	if(S->top)
		node->next=S->top;
	S->top=node;
}

int Pop(Stack *S){
	int data=-1;
	List *temp;
	if(isEmptyStack(S))
		return data;
	else{
		temp=S->top;
		if(S->top->next)
			S->top=S->top->next;
		else
			S->top=NULL;
		data=temp->data;
		free(temp);
		return data;
	}
}

int Top(Stack *S){
	if(S->top)
		return S->top->data;
	else
		return -1;
}

int isEmptyStack(Stack *S){
	return (S->top==NULL);
}

void DeleteStack(Stack *S){
	List *temp;
	while(S->top){
		temp = S->top;
		S->top=S->top->next;
		free(temp);
	}
	free(S);
}
