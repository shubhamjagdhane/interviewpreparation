def get_Ratios(l1,l2):
	ratios=[]
	try:
		l1=list(map(lambda x: int(x),l1))
		l2=list(map(lambda x: int(x),l2))
		for index in range(len(l1)):
			ratios.append(l1[index]/l2[index])
	except ZeroDivisionError:
		ratios.append(float('nan')) #nan=not a number
	except TypeError:
		raise TypeError("Invalid Type,Numbers Excepted in a List")
	except ValueError:
		raise ValueError('get_Ratios called with bad arg')
	except:
		raise("Some this bad happened")
	return ratios

if __name__=='__main__':
	l1=list(map(str,input().split()))
	l2=list(map(str,input().split()))
	#l1=[1,2,3]
	#l2=[4,5,6]
	print(get_Ratios(l1,l2))
