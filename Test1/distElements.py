def getDistinct(arr,n):
	try:
		if type(n)!=int or type(arr)!=list:
			raise TypeError()
		if n==0 and arr!=[]:
			raise ValueError()
		d={}
		for i in arr:
			if(d.get(i)):
				d[i]+=1
			else:
				d[i]=1
		distinct=list(d.keys())
		return distinct
	except TypeError:
		print("\nInvalid Type, Need Integer type for size of list and list type to represent the collection of a numbers")

	except ValueError:
		print("Invalid Value,If n=0 then list should be empty")
	except:
		print("Something went wrong")
if __name__=='__main__':
	t=int(input("Number of test cases: "))
	for _ in range(t):
		n=int(input("How many elements: "))
		arr=list(map(int,input().split()))
		ans=getDistinct(arr,n)
		print(ans if ans else "")

