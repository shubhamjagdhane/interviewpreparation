def makeDictionary(s):
	d={}
	for i in s:
		if d.get(i):
			d[i]+=1
		else:
			d[i]=1

	return sorted(list(d.keys()))

def anagram(s1,s2):
	l1=makeDictionary(s1)
	l2=makeDictionary(s2)
	if(l1==l2):
		return True
	else:
		return False

if __name__=='__main__':
	str1=input()
	str2=input()
	if anagram(str1,str2):
		print("Anagram")
	else:
		print("Not Anagram")
