import unittest
from fact import findFact

class Test_Find_Fact(unittest.TestCase):
	def test_correct_values(self):
		assert findFact(5)==120,"Incorrect Value"

	def test_invalid_type(self):
		try:
			result=findFact("5") 
			result=findFact([1]) 
			result=findFact(1.0) 
			result=findFact((1,2))
		except Exception as e:
			assert isinstance(e,TypeError)
	def test_negative_values(self):
		try:
			result=findFact(-12)
		except Exception as e:
			assert isinstance(e,ValueError)
	def test_some_wrong(self):
		try:
			result=findFact(abc12)
		except Exception as e:
			print("Something Went very Wrong:",e)

