n = int(input())
for _ in range(n):
	lsize=int(input())
	l1=input().split()
	list1 = list(map(int,l1))
	l=len(list1)
	i=0
	while i<l-1 and list1[i]>=list1[i+1]:
		i+=1
	while i<l-1 and list1[i]<=list1[i+1]:
		i+=1
	print("Yes" if i==l-1 else "No")
