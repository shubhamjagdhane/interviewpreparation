#include<stdio.h>
#include<stdlib.h>
#include "linkedList.h"

void createRing(List *head,int pos);
int getLast5Digits(long int num,int n);
int isRingPresent(List *head);
int power(int a,int b);

int main(void){
	int totEle,pos;
	printf("\nEnter how many elements: ");
	scanf("%d",&totEle);
	

	List *head=createList(totEle);
	printf("\nYour created List: ");
	display(head);

	
	printf("\nEnter the position from where to start ring: ");
	scanf("%d",&pos);
	createRing(head,pos);

	if(isRingPresent(head))
		printf("\nRing is present");
	else{
		printf("\nRing is not present");
		deleteList(&head);
	}

	printf("\n\n");

}

int isRingPresent(List *head){

	int *arr=(int *)malloc(sizeof(int)*100);

	List *temp=head;

	int flag=0,i;

	while(i!=0 && temp!=NULL){
		i=getLast5Digits((long int)temp->next,5);
		printf("\n %d = %d",i,i%101);
		i=i%101;
		if(arr[i]==1){
			flag=1;
			break;
		}
		else
			arr[i]=1;
		temp=temp->next;
	}
	free(arr);
	return flag;
}

int getLast5Digits(long int num,int n){
	if(n==0 || num==0)
		return 0;
	else
		return (getLast5Digits(num/10,n-1)+((num%10)*power(10,5-n)));
}

int power(int a,int b){
	if(b==0)
		return 1;
	else
		return (a*power(a,b-1));
}

void createRing(List *head,int pos){
	List *temp1=head,*temp2;
	for(int i=0 ; i<pos ; i++){
		if(temp1)
			temp1=temp1->next;
	}
	if(!temp1)
		return;
	temp2=temp1;
	while(temp2->next!=NULL)
		temp2=temp2->next;
	temp2->next=temp1;
}
