#include<stdio.h>
#include<stdlib.h>

typedef struct StructLinkedList{
	int data;
	struct StructLinkedList *next;
}List;

void deleteList(List **head);
void display(List *head);
List *createList(int n);

void deleteList(List **head){
	List *temp=*head;
	while(temp!=NULL){
		*head=(*head)->next;
		free(temp);
		temp=*head;
	}
}

List *createList(int n){
	List *head=NULL,*temp,*newNode;
	for(int i=0 ; i<n ; i++){
		newNode= (List *)malloc(sizeof(List));
		printf("\nEnter the data for %d node: ",i+1);
		scanf("%d",&newNode->data);
		newNode->next=NULL;

		if(head==NULL)
			head=newNode;
		else{
			temp=head;
			while(temp->next!=NULL)
				temp=temp->next;
			temp->next=newNode;
		}
	}
	return head;
}

void display(List *head){
	while(head!=NULL){
		printf("%d ",head->data);
		head=head->next;
	}
	printf("\n\n");
}
