#include<stdio.h>
#include<stdlib.h>
#include "linkedList.h"

List *getNthNodeFromEndSolution1(List *head,int n);
List *getNthNodeFromEndSolution2(List *head,int n);

int main(void){
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);

	List *head;
	head=createList(n);
	printf("\nYour Linked List: ");
	display(head);

	printf("\nEnter which node you want from end: ");
	scanf("%d",&n);

	List *nthNode=getNthNodeFromEndSolution1(head,n);
	
	printf("\nThis is solution by method 1: ");
	if(nthNode)
		printf("\n%dth node from end= %d",n,nthNode->data);
	else
		printf("\n%d nth end node is does not exist in the list...!",n);

	nthNode=getNthNodeFromEndSolution2(head,n);

	printf("\nThis is solution by method 2: ");
	if(nthNode)
		printf("\n%dth node from end= %d",n,nthNode->data);
	else
		printf("\n%d nth end node is does not exist in the list...!",n);

	
	deleteList(&head);
	printf("\n\n");
}

List *getNthNodeFromEndSolution1(List *head,int n){
	int totNodes=0;
	List *temp=head;
	while(temp!=NULL){
		totNodes+=1;
		temp=temp->next;
	}
	
	int travel=totNodes-n+1;
	if(travel<=0)
		return NULL;
	temp=head;
	int i=1;
	while(i<travel){
		i+=1;
		if(temp)
			temp=temp->next;
	}
	return temp;
}


List *getNthNodeFromEndSolution2(List *head,int n){
	List *temp=head,*prevTemp=NULL;
	for(int i=0 ; i<n ; i++)
		if(temp)
			temp=temp->next;
	if(!temp)
		return NULL;
	prevTemp=head;
	while(temp){
		prevTemp=prevTemp->next;
		temp=temp->next;
	}
	return prevTemp;
}
