#include<stdio.h>
#include<stdlib.h>
#include "queueForTree.h"

typedef struct StructTreeForTest{
	int data;
	struct StructTreeForTest *left,*right;
}TreeTest;

typedef struct StructListStack{
	TreeTest *node;
	struct StructListStack *next;
}ListStack;

typedef struct StructStack{
	ListStack *top;
}Stack;


int isEmptyStack(Stack *S);
TreeTest *Top(Stack *S);
TreeTest *Pop(Stack *S);
void Push(Stack *S,TreeTest *node);
void DeleteStack(Stack *S);
Stack *createStack(void);
void PreOrderNonRecurrsive(TreeTest *root);
void InOrderNonRecurrsive(TreeTest *root);
void InOrderRecurrsive(TreeTest *root);

int main(void){
	int data;
	TreeTest *root=NULL;
	printf("\nEnter how many nodes for tree: ");
	scanf("%d",&data);
	root=(TreeTest*)createTreeByLevelOrder(data);
	if (!root){
		printf("\nRoot is not created..!!");
		exit(0);
	}

	printf("\nroot->data=%d",root->data);
	printf("\nroot->left->data=%d",root->left->data);
	printf("\nroot->right->data=%d",root->right->data);


	PreOrderNonRecurrsive(root);
	InOrderNonRecurrsive(root);
//	printf("\nInOrderRecurrsive: ");
//	InOrderRecurrsive(root);
	printByLevelOrder((Tree *)root);
	DeleteTree((Tree *)root);
	printf("\n\n");
}


Stack *createStack(void){
	Stack *S=(Stack *)malloc(sizeof(Stack));
	ListStack *temp=(ListStack *)malloc(sizeof(ListStack));
	S->top=NULL;
	return S;
}

void Push(Stack *S,TreeTest *node){
	ListStack *listNode=(ListStack *)malloc(sizeof(ListStack));
	listNode->node=node;
	listNode->next=NULL;
	
	if(S->top)
		listNode->next=S->top;
	S->top=listNode;
}

TreeTest *Pop(Stack *S){
	int data=-1;
	ListStack *temp;
	TreeTest *tempNode;
	if(isEmptyStack(S))
		return NULL;
	else{
		temp=S->top;
		if(S->top->next)
			S->top=S->top->next;
		else
			S->top=NULL;
		tempNode=temp->node;
		free(temp);
		return tempNode;
	}
}

TreeTest *Top(Stack *S){
	if(S->top)
		return S->top->node;
	else
		return NULL;
}

int isEmptyStack(Stack *S){
	return (S->top==NULL);
}

void DeleteStack(Stack *S){
	ListStack *temp;
	while(S->top){
		temp = S->top;
		S->top=S->top->next;
		free(temp);
	}
	free(S);
}

void PreOrderNonRecurrsive(TreeTest *root){
	Stack *S=createStack();
	TreeTest *temp=root;
	printf("\nPreOrder-Non-Recurrsive: ");
	while(1){
		while(temp){
			Push(S,temp);
			printf("%d ",temp->data);
			temp=temp->left;
		}
		if(isEmptyStack(S))
			break;
		temp=Pop(S);
		temp=temp->right;
	}
	DeleteStack(S);
}

void InOrderRecurrsive(TreeTest *root){
	TreeTest *temp=root;
	if(temp){
		InOrderRecurrsive(temp->left);
		printf("%d ",temp->data);
		InOrderRecurrsive(temp->right);
	}
}

void InOrderNonRecurrsive(TreeTest *root){
	TreeTest *temp=root;
	Stack *S=createStack();
	printf("\nInOrder-Non-Recurrsive: ");
	while(1){
		while(temp){
			Push(S,temp);
			temp=temp->left;
		}
		if(isEmptyStack(S))
			break;
		temp=Pop(S);
		printf("%d ",temp->data);
		temp=temp->right;
	}
	DeleteStack(S);
}
