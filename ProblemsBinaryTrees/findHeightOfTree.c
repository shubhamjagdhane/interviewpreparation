#include "TreeWithStackQueue.h"

int findHeightOfTree(Tree *root);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	n=findHeightOfTree(root);
	printf("\nHeight of tree= %d",n);
	printf("\n\n");
}

int findHeightOfTree(Tree *root){
	int max=-1,left=0,right=0;
	if(root==NULL)
		return 0;
	else{
		left=1+findHeightOfTree(root->left);
		right=1+findHeightOfTree(root->right);
		if(left>right)
			return left;
		else
			return right;
	}
}
