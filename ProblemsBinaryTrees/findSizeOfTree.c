#include "TreeWithStackQueue.h"

int findSizeOfTree(Tree *root);
int findSizeOfTreeNonRecurrsive(Tree *root);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	printByLevelOrder(root);
	n=findSizeOfTree(root);
	printf("\nSize of Tree= %d",n);
	n=findSizeOfTreeNonRecurrsive(root);
	printf("\nSize of Tree by NonRecurrsive= %d",n);
	printf("\n\n");
}

int findSizeOfTree(Tree *root){
	if(root==NULL)
		return 0;
	else{
		return (findSizeOfTree(root->left)+1+findSizeOfTree(root->right));
	}
}

int findSizeOfTreeNonRecurrsive(Tree *root){
	Tree *temp;
	Queue *Q=createQueue();
	EnQueue(Q,root);
	int cnt=0;
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		cnt++;
		if (temp->left)
			EnQueue(Q,temp->left);
		if (temp->right)
			EnQueue(Q,temp->right);
	}
	return cnt;
}
