#include "TreeWithStackQueue.h"

void insertIntoTree(Tree *root,int data);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	printByLevelOrder(root);
	printf("\nEnter an element to insert: ");
	scanf("%d",&n);
	insertIntoTree(root,n);
	printByLevelOrder(root);
	printf("\n\n");
}

void insertIntoTree(Tree *root,int data){
	Tree *newNode=(Tree *)malloc(sizeof(Tree));
	Tree *temp;
	newNode->data=data;
	newNode->left=NULL;
	newNode->right=NULL;

	if (!root){
		root=newNode;
		return;
	}

	Queue *Q=createQueue();
	EnQueue(Q,root);
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		if(temp->left)
			EnQueue(Q,temp->left);
		else{
			temp->left=newNode;
			DeleteQueue(Q);
			return;
		}
		if(temp->right)
			EnQueue(Q,temp->right);
		else{
			temp->right=newNode;
			DeleteQueue(Q);
			return;
		}
	}
}
