#include<stdio.h>
#include<stdlib.h>

typedef struct StructTree{
	int data;
	struct StructTree *right,*left;
}Tree;

typedef struct StructList{
	Tree *node;
	struct StructList *next;
}List;

typedef struct StructQueue{
	List *front,*rear;
}Queue;

Queue *createQueue(void);
Tree *createTreeByLevelOrder(int n);
void EnQueue(Queue *Q,Tree *node);
Tree *getFirstElementFromQueue(Queue *Q);
Tree *DeQueue(Queue *Q);
void DeleteQueue(Queue *Q);
void printByLevelOrder(Tree *root);
int isEmptyQueue(Queue *Q);
void DeleteTree(Tree *root);
void Inorder(Tree *root);

typedef struct StructStack{
	List *top;
}Stack;


int isEmptyStack(Stack *S);
Tree *Top(Stack *S);
Tree *Pop(Stack *S);
void Push(Stack *S,Tree *node);
void DeleteStack(Stack *S);
Stack *createStack(void);
void PreOrderNonRecurrsive(Tree *root);
void InOrderNonRecurrsive(Tree *root);
void InOrderRecurrsive(Tree *root);
void PostOrderNonRecurrsive(Tree *root);

/*int main(void){
	int data;
	Tree *root=NULL;
	printf("\nEnter how many nodes for tree: ");
	scanf("%d",&data);
	root=(Tree*)createTreeByLevelOrder(data);
	if (!root){
		printf("\nRoot is not created..!!");
		exit(0);
	}

	printf("\nroot->data=%d",root->data);
	printf("\nroot->left->data=%d",root->left->data);
	printf("\nroot->right->data=%d",root->right->data);


	PreOrderNonRecurrsive(root);
	InOrderNonRecurrsive(root);
	printf("\nInOrderRecurrsive: ");
	InOrderRecurrsive(root);
	PostOrderNonRecurrsive(root);
	printByLevelOrder(root);
	DeleteTree(root);
	printf("\n\n");
}
*/

Stack *createStack(void){
	Stack *S=(Stack *)malloc(sizeof(Stack));
	List *temp=(List *)malloc(sizeof(List));
	S->top=NULL;
	return S;
}

void Push(Stack *S,Tree *node){
	List *listNode=(List *)malloc(sizeof(List));
	listNode->node=node;
	listNode->next=NULL;
	
	if(S->top)
		listNode->next=S->top;
	S->top=listNode;
}

Tree *Pop(Stack *S){
	int data=-1;
	List *temp;
	Tree *tempNode;
	if(isEmptyStack(S))
		return NULL;
	else{
		temp=S->top;
		if(S->top->next)
			S->top=S->top->next;
		else
			S->top=NULL;
		tempNode=temp->node;
		free(temp);
		return tempNode;
	}
}

Tree *Top(Stack *S){
	if(S->top)
		return S->top->node;
	else
		return NULL;
}

int isEmptyStack(Stack *S){
	return (S->top==NULL);
}

void DeleteStack(Stack *S){
	List *temp;
	while(S->top){
		temp = S->top;
		S->top=S->top->next;
		free(temp);
	}
	free(S);
}

void PreOrderNonRecurrsive(Tree *root){
	Stack *S=createStack();
	printf("\nPreOrder-Non-Recurrsive: ");
	while(1){
		while(root){
			printf("%d ",root->data);
			Push(S,root);
			root=root->left;
		}
		if(isEmptyStack(S))
			break;
		root=Pop(S);
		root=root->right;
	}
	DeleteStack(S);
}

void InOrderRecurrsive(Tree *root){
	Tree *temp=root;
	if(temp){
		InOrderRecurrsive(temp->left);
		printf("%d ",temp->data);
		InOrderRecurrsive(temp->right);
	}
}

void InOrderNonRecurrsive(Tree *root){
	Stack *S=createStack();
	printf("\nInOrder-Non-Recurrsive: ");
	while(1){
		while(root){
			Push(S,root);
			root=root->left;
		}
		if(isEmptyStack(S))
			break;
		root=Pop(S);
		printf("%d ",root->data);
		root=root->right;
	}
	DeleteStack(S);
}

Queue *createQueue(void){
	Queue *Q=(Queue *)malloc(sizeof(Queue));
	List *temp=(List *)malloc(sizeof(List));
	Q->front=Q->rear=NULL;
	return Q;
}

Tree *createTreeByLevelOrder(int n){
	Queue *Q=createQueue();
	Tree *root=NULL,*newNode,*temp;
	for(int i=0 ; i<n ; i++){
		newNode=(Tree *)malloc(sizeof(Tree));
		printf("\nEnter data for Node: ");
		scanf("%d",&newNode->data);
		newNode->left=NULL;
		newNode->right=NULL;
		EnQueue(Q,newNode);
		temp=getFirstElementFromQueue(Q);
		if(root==NULL){
			root=newNode;
		}
		else if(temp->left==NULL){
				temp->left=newNode;
		}
		else{
				temp->right=newNode;
				DeQueue(Q);
		}
	}
	DeleteQueue(Q);
	return root;
}

void EnQueue(Queue *Q,Tree *node){
	List *listNodes=(List *)malloc(sizeof(List));
	listNodes->node=node;
	listNodes->next=NULL;
	if(Q->rear)
		Q->rear->next=listNodes;
	Q->rear=listNodes;
	if(Q->front==NULL){
		Q->front=Q->rear;
	}
}

Tree *getFirstElementFromQueue(Queue *Q){
	if(Q->front)
		return (Q->front->node);
	else
		return NULL;
}

Tree *DeQueue(Queue *Q){
	List *tempNode;
	Tree *temp;
	tempNode=Q->front;
	temp=tempNode->node;
	if(Q->front->next)
		Q->front=Q->front->next;
	else
		Q->front=Q->rear=NULL;
	free(tempNode);
	return temp;
}

void DeleteQueue(Queue *Q){
	List *temp;
	while(Q->front){
		temp=Q->front;
		Q->front=Q->front->next;
		free(temp);
	}
	free(Q->front);
}

void printByLevelOrder(Tree *root){
	Queue *Q=createQueue();
	Tree *temp;
	EnQueue(Q,root);
	printf("\nLevel Order Print: ");
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		printf("%d ",temp->data);
		if(temp->left){
			EnQueue(Q,temp->left);
		}
		if(temp->right){
			EnQueue(Q,temp->right);
		}
	}
	DeleteQueue(Q);
}

int isEmptyQueue(Queue *Q){
	return (Q->front==NULL);
}

void DeleteTree(Tree *root){
	if(root==NULL)
		return;
	DeleteTree(root->left);
	DeleteTree(root->right);
	free(root);
}

void Inorder(Tree *root){
	if(root){
		Inorder(root->left);
		printf("%d ",root->data);
		Inorder(root->right);
	}
}
void PostOrderNonRecurrsive(Tree *root){
	Stack *S=createStack();
	Tree *prev=NULL,*current;
	Push(S,root);
	printf("\nPostOrderNonRecurrsive: ");
	while(!isEmptyStack(S)){
		current=Pop(S);
		if(!prev || prev->left==current || prev->right==current){
			printf("\nparent=prev");
			if(current->left)
				Push(S,current->left);
			else if(current->right)
				Push(S,current->right);
		}
		else if(current->left==prev){
			printf("\nparent=current");
			if(current->right)
				Push(S,current->right);
		}
		else{
			printf("\nprinting data");
			printf("%d ",current->data);
			Pop(S);
		}
		prev=current;
	}
}
