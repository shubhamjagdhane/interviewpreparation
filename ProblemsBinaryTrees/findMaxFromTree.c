#include "TreeWithStackQueue.h"

int findMaxFromTree(Tree *root);
int findMaxFromTreeNonRecurrsive(Tree *root);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	printByLevelOrder(root);
	n=findMaxFromTree(root);
	printf("\nMaximum from tree is= %d",n);
	n=findMaxFromTreeNonRecurrsive(root);
	printf("\nMaximum from tree by nonrecurrsive= %d",n);
	printf("\n\n");
	DeleteTree(root);
}

int findMaxFromTree(Tree *root){
	int root_val,left,right,max=-1;
	if(root!=NULL){
		root_val=root->data;
		left=findMaxFromTree(root->left);
		right=findMaxFromTree(root->right);

		if(left>right)
			max=left;
		else
			max=right;
		if(root_val>max)
			max=root_val;
	}
	return max;
}

int findMaxFromTreeNonRecurrsive(Tree *root){
	int max=-1;
	if(!root)
		return max;
	Queue *Q=createQueue();
	Tree *temp;
	EnQueue(Q,root);
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		if (temp->data > max)
			max=temp->data;
		if (temp->left)
			EnQueue(Q,temp->left);
		if (temp->right)
			EnQueue(Q,temp->right);
	}
	DeleteQueue(Q);
	return max;
}
