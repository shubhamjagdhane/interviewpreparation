#include "TreeWithStackQueue.h"

void LevelOrderReverse(Tree *root);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nEnter how many nodes: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	printByLevelOrder(root);
	LevelOrderReverse(root);
	printf("\n\n");
}
void LevelOrderReverse(Tree *root){
	Tree *temp;
	Queue *Q=createQueue();
	Stack *S=createStack();
	EnQueue(Q,root);
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		Push(S,temp);
		if(temp->right)
			EnQueue(Q,temp->right);
		if(temp->left)
			EnQueue(Q,temp->left);
	}
	printf("\nLevel Order Reverse: ");
	while(!isEmptyStack(S)){
		temp=Pop(S);
		printf("%d ",temp->data);
	}
}
