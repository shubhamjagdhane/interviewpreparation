#include "TreeWithStackQueue.h"

int searchElementFromTree(Tree *root,int data);
int searchElementFromTreeNonRecurrsive(Tree *root,int data);
int main(void){
	Tree *root=NULL;
	int n;
	printf("\nHow many nodes you want in tree: ");
	scanf("%d",&n);
	root=createTreeByLevelOrder(n);
	printf("\nEnter the search element: ");
	scanf("%d",&n);
	int flag=0;
	printf("\nBy Recurrsive Way: ");
	flag=searchElementFromTree(root,n);
	if (flag)
		printf("\n%d Element is present in tree.",n);
	else
		printf("\n%d element is found not in tree.",n);

	printf("\nBy Non-Recurrsive Way: ");
	flag=searchElementFromTreeNonRecurrsive(root,n);
	if (flag)
		printf("\n%d Element is present in tree.",n);
	else
		printf("\n%d element is found not in tree.",n);


	DeleteTree(root);
	printf("\n\n");
}

int searchElementFromTree(Tree *root,int data){
	int present=0;
	if (root==NULL)
		return 0;
	else{
		if (root->data==data)
			return 1;
		else{
			present=searchElementFromTree(root->left,data);
			if (present)
				return present;
			else
				return (searchElementFromTree(root->right,data));
		}
	}
}

int searchElementFromTreeNonRecurrsive(Tree *root,int data){
	int flag=0;
	Queue *Q=createQueue();
	EnQueue(Q,root);
	Tree *temp;
	while(!isEmptyQueue(Q)){
		temp=DeQueue(Q);
		if (temp->data==data){
			flag=1;
			break;
		}
		if(temp->left)
			EnQueue(Q,temp->left);
		if(temp->right)
			EnQueue(Q,temp->right);
	}
	DeleteQueue(Q);
	return flag;
}
